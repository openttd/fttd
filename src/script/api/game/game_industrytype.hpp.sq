/* $Id$ */

/*
 * This file is part of OpenTTD.
 * OpenTTD is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 2.
 * OpenTTD is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details. You should have received a copy of the GNU General Public License along with OpenTTD. If not, see <http://www.gnu.org/licenses/>.
 */

/* THIS FILE IS AUTO-GENERATED; PLEASE DO NOT ALTER MANUALLY */

#include "../script_industrytype.hpp"


static void SQGSIndustryType_Register (Squirrel *engine)
{
	engine->AddClassBegin ("GSIndustryType");
	SQConvert::AddConstructor <void (ScriptIndustryType::*)(), 1> (engine, "x");

	engine->AddConst ("INDUSTRYTYPE_UNKNOWN", ScriptIndustryType::INDUSTRYTYPE_UNKNOWN);
	engine->AddConst ("INDUSTRYTYPE_TOWN",    ScriptIndustryType::INDUSTRYTYPE_TOWN);

	SQConvert::DefSQStaticMethod (engine, &ScriptIndustryType::IsValidIndustryType,   "IsValidIndustryType",   2, ".i");
	SQConvert::DefSQStaticMethod (engine, &ScriptIndustryType::GetName,               "GetName",               2, ".i");
	SQConvert::DefSQStaticMethod (engine, &ScriptIndustryType::GetProducedCargo,      "GetProducedCargo",      2, ".i");
	SQConvert::DefSQStaticMethod (engine, &ScriptIndustryType::GetAcceptedCargo,      "GetAcceptedCargo",      2, ".i");
	SQConvert::DefSQStaticMethod (engine, &ScriptIndustryType::IsRawIndustry,         "IsRawIndustry",         2, ".i");
	SQConvert::DefSQStaticMethod (engine, &ScriptIndustryType::IsProcessingIndustry,  "IsProcessingIndustry",  2, ".i");
	SQConvert::DefSQStaticMethod (engine, &ScriptIndustryType::ProductionCanIncrease, "ProductionCanIncrease", 2, ".i");
	SQConvert::DefSQStaticMethod (engine, &ScriptIndustryType::GetConstructionCost,   "GetConstructionCost",   2, ".i");
	SQConvert::DefSQStaticMethod (engine, &ScriptIndustryType::CanBuildIndustry,      "CanBuildIndustry",      2, ".i");
	SQConvert::DefSQStaticMethod (engine, &ScriptIndustryType::CanProspectIndustry,   "CanProspectIndustry",   2, ".i");
	SQConvert::DefSQStaticMethod (engine, &ScriptIndustryType::BuildIndustry,         "BuildIndustry",         3, ".ii");
	SQConvert::DefSQStaticMethod (engine, &ScriptIndustryType::ProspectIndustry,      "ProspectIndustry",      2, ".i");
	SQConvert::DefSQStaticMethod (engine, &ScriptIndustryType::IsBuiltOnWater,        "IsBuiltOnWater",        2, ".i");
	SQConvert::DefSQStaticMethod (engine, &ScriptIndustryType::HasHeliport,           "HasHeliport",           2, ".i");
	SQConvert::DefSQStaticMethod (engine, &ScriptIndustryType::HasDock,               "HasDock",               2, ".i");

	engine->AddClassEnd();
}
