/* $Id$ */

/*
 * This file is part of OpenTTD.
 * OpenTTD is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 2.
 * OpenTTD is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details. You should have received a copy of the GNU General Public License along with OpenTTD. If not, see <http://www.gnu.org/licenses/>.
 */

/* THIS FILE IS AUTO-GENERATED; PLEASE DO NOT ALTER MANUALLY */

#include "../script_stationlist.hpp"


static void SQGSStationList_Register (Squirrel *engine)
{
	engine->AddClassBegin ("GSStationList", "GSList");
	SQConvert::AddConstructor <void (ScriptStationList::*)(ScriptStation::StationType station_type), 2> (engine, "xi");

	engine->AddClassEnd();
}


static void SQGSStationList_Cargo_Register (Squirrel *engine)
{
	engine->AddClassBegin ("GSStationList_Cargo", "GSList");
	SQConvert::AddConstructor <void (ScriptStationList_Cargo::*)(ScriptStationList_Cargo::CargoMode mode, ScriptStationList_Cargo::CargoSelector selector, StationID station_id, CargoID cargo, StationID other_station), 6> (engine, "xiiiii");

	engine->AddConst ("CS_BY_FROM",     ScriptStationList_Cargo::CS_BY_FROM);
	engine->AddConst ("CS_VIA_BY_FROM", ScriptStationList_Cargo::CS_VIA_BY_FROM);
	engine->AddConst ("CS_BY_VIA",      ScriptStationList_Cargo::CS_BY_VIA);
	engine->AddConst ("CS_FROM_BY_VIA", ScriptStationList_Cargo::CS_FROM_BY_VIA);
	engine->AddConst ("CM_WAITING",     ScriptStationList_Cargo::CM_WAITING);
	engine->AddConst ("CM_PLANNED",     ScriptStationList_Cargo::CM_PLANNED);

	engine->AddClassEnd();
}


static void SQGSStationList_CargoWaiting_Register (Squirrel *engine)
{
	engine->AddClassBegin ("GSStationList_CargoWaiting", "GSStationList_Cargo");
	SQConvert::AddConstructor <void (ScriptStationList_CargoWaiting::*)(ScriptStationList_Cargo::CargoSelector selector, StationID station_id, CargoID cargo, StationID other_station), 5> (engine, "xiiii");

	engine->AddClassEnd();
}


static void SQGSStationList_CargoPlanned_Register (Squirrel *engine)
{
	engine->AddClassBegin ("GSStationList_CargoPlanned", "GSStationList_Cargo");
	SQConvert::AddConstructor <void (ScriptStationList_CargoPlanned::*)(ScriptStationList_Cargo::CargoSelector selector, StationID station_id, CargoID cargo, StationID other_station), 5> (engine, "xiiii");

	engine->AddClassEnd();
}


static void SQGSStationList_CargoWaitingByFrom_Register (Squirrel *engine)
{
	engine->AddClassBegin ("GSStationList_CargoWaitingByFrom", "GSStationList_CargoWaiting");
	SQConvert::AddConstructor <void (ScriptStationList_CargoWaitingByFrom::*)(StationID station_id, CargoID cargo), 3> (engine, "xii");

	engine->AddClassEnd();
}


static void SQGSStationList_CargoWaitingViaByFrom_Register (Squirrel *engine)
{
	engine->AddClassBegin ("GSStationList_CargoWaitingViaByFrom", "GSStationList_CargoWaiting");
	SQConvert::AddConstructor <void (ScriptStationList_CargoWaitingViaByFrom::*)(StationID station_id, CargoID cargo, StationID via), 4> (engine, "xiii");

	engine->AddClassEnd();
}


static void SQGSStationList_CargoWaitingByVia_Register (Squirrel *engine)
{
	engine->AddClassBegin ("GSStationList_CargoWaitingByVia", "GSStationList_CargoWaiting");
	SQConvert::AddConstructor <void (ScriptStationList_CargoWaitingByVia::*)(StationID station_id, CargoID cargo), 3> (engine, "xii");

	engine->AddClassEnd();
}


static void SQGSStationList_CargoWaitingFromByVia_Register (Squirrel *engine)
{
	engine->AddClassBegin ("GSStationList_CargoWaitingFromByVia", "GSStationList_CargoWaiting");
	SQConvert::AddConstructor <void (ScriptStationList_CargoWaitingFromByVia::*)(StationID station_id, CargoID cargo, StationID from), 4> (engine, "xiii");

	engine->AddClassEnd();
}


static void SQGSStationList_CargoPlannedByFrom_Register (Squirrel *engine)
{
	engine->AddClassBegin ("GSStationList_CargoPlannedByFrom", "GSStationList_CargoPlanned");
	SQConvert::AddConstructor <void (ScriptStationList_CargoPlannedByFrom::*)(StationID station_id, CargoID cargo), 3> (engine, "xii");

	engine->AddClassEnd();
}


static void SQGSStationList_CargoPlannedViaByFrom_Register (Squirrel *engine)
{
	engine->AddClassBegin ("GSStationList_CargoPlannedViaByFrom", "GSStationList_CargoPlanned");
	SQConvert::AddConstructor <void (ScriptStationList_CargoPlannedViaByFrom::*)(StationID station_id, CargoID cargo, StationID via), 4> (engine, "xiii");

	engine->AddClassEnd();
}


static void SQGSStationList_CargoPlannedByVia_Register (Squirrel *engine)
{
	engine->AddClassBegin ("GSStationList_CargoPlannedByVia", "GSStationList_CargoPlanned");
	SQConvert::AddConstructor <void (ScriptStationList_CargoPlannedByVia::*)(StationID station_id, CargoID cargo), 3> (engine, "xii");

	engine->AddClassEnd();
}


static void SQGSStationList_CargoPlannedFromByVia_Register (Squirrel *engine)
{
	engine->AddClassBegin ("GSStationList_CargoPlannedFromByVia", "GSStationList_CargoPlanned");
	SQConvert::AddConstructor <void (ScriptStationList_CargoPlannedFromByVia::*)(StationID station_id, CargoID cargo, StationID from), 4> (engine, "xiii");

	engine->AddClassEnd();
}


static void SQGSStationList_Vehicle_Register (Squirrel *engine)
{
	engine->AddClassBegin ("GSStationList_Vehicle", "GSList");
	SQConvert::AddConstructor <void (ScriptStationList_Vehicle::*)(VehicleID vehicle_id), 2> (engine, "xi");

	engine->AddClassEnd();
}
