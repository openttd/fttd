/* $Id$ */

/*
 * This file is part of OpenTTD.
 * OpenTTD is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 2.
 * OpenTTD is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details. You should have received a copy of the GNU General Public License along with OpenTTD. If not, see <http://www.gnu.org/licenses/>.
 */

/* THIS FILE IS AUTO-GENERATED; PLEASE DO NOT ALTER MANUALLY */

#include "../script_error.hpp"


static void SQAIError_Register (Squirrel *engine)
{
	engine->AddClassBegin ("AIError");
	SQConvert::AddConstructor <void (ScriptError::*)(), 1> (engine, "x");

	engine->AddConst ("ERR_CAT_NONE",                         ScriptError::ERR_CAT_NONE);
	engine->AddConst ("ERR_CAT_GENERAL",                      ScriptError::ERR_CAT_GENERAL);
	engine->AddConst ("ERR_CAT_VEHICLE",                      ScriptError::ERR_CAT_VEHICLE);
	engine->AddConst ("ERR_CAT_STATION",                      ScriptError::ERR_CAT_STATION);
	engine->AddConst ("ERR_CAT_BRIDGE",                       ScriptError::ERR_CAT_BRIDGE);
	engine->AddConst ("ERR_CAT_TUNNEL",                       ScriptError::ERR_CAT_TUNNEL);
	engine->AddConst ("ERR_CAT_TILE",                         ScriptError::ERR_CAT_TILE);
	engine->AddConst ("ERR_CAT_SIGN",                         ScriptError::ERR_CAT_SIGN);
	engine->AddConst ("ERR_CAT_RAIL",                         ScriptError::ERR_CAT_RAIL);
	engine->AddConst ("ERR_CAT_ROAD",                         ScriptError::ERR_CAT_ROAD);
	engine->AddConst ("ERR_CAT_ORDER",                        ScriptError::ERR_CAT_ORDER);
	engine->AddConst ("ERR_CAT_MARINE",                       ScriptError::ERR_CAT_MARINE);
	engine->AddConst ("ERR_CAT_WAYPOINT",                     ScriptError::ERR_CAT_WAYPOINT);
	engine->AddConst ("ERR_CAT_BIT_SIZE",                     ScriptError::ERR_CAT_BIT_SIZE);
	engine->AddConst ("ERR_NONE",                             ScriptError::ERR_NONE);
	engine->AddConst ("ERR_UNKNOWN",                          ScriptError::ERR_UNKNOWN);
	engine->AddConst ("ERR_PRECONDITION_FAILED",              ScriptError::ERR_PRECONDITION_FAILED);
	engine->AddConst ("ERR_PRECONDITION_STRING_TOO_LONG",     ScriptError::ERR_PRECONDITION_STRING_TOO_LONG);
	engine->AddConst ("ERR_PRECONDITION_TOO_MANY_PARAMETERS", ScriptError::ERR_PRECONDITION_TOO_MANY_PARAMETERS);
	engine->AddConst ("ERR_PRECONDITION_INVALID_COMPANY",     ScriptError::ERR_PRECONDITION_INVALID_COMPANY);
	engine->AddConst ("ERR_NEWGRF_SUPPLIED_ERROR",            ScriptError::ERR_NEWGRF_SUPPLIED_ERROR);
	engine->AddConst ("ERR_GENERAL_BASE",                     ScriptError::ERR_GENERAL_BASE);
	engine->AddConst ("ERR_NOT_ENOUGH_CASH",                  ScriptError::ERR_NOT_ENOUGH_CASH);
	engine->AddConst ("ERR_LOCAL_AUTHORITY_REFUSES",          ScriptError::ERR_LOCAL_AUTHORITY_REFUSES);
	engine->AddConst ("ERR_ALREADY_BUILT",                    ScriptError::ERR_ALREADY_BUILT);
	engine->AddConst ("ERR_AREA_NOT_CLEAR",                   ScriptError::ERR_AREA_NOT_CLEAR);
	engine->AddConst ("ERR_OWNED_BY_ANOTHER_COMPANY",         ScriptError::ERR_OWNED_BY_ANOTHER_COMPANY);
	engine->AddConst ("ERR_NAME_IS_NOT_UNIQUE",               ScriptError::ERR_NAME_IS_NOT_UNIQUE);
	engine->AddConst ("ERR_FLAT_LAND_REQUIRED",               ScriptError::ERR_FLAT_LAND_REQUIRED);
	engine->AddConst ("ERR_LAND_SLOPED_WRONG",                ScriptError::ERR_LAND_SLOPED_WRONG);
	engine->AddConst ("ERR_VEHICLE_IN_THE_WAY",               ScriptError::ERR_VEHICLE_IN_THE_WAY);
	engine->AddConst ("ERR_SITE_UNSUITABLE",                  ScriptError::ERR_SITE_UNSUITABLE);
	engine->AddConst ("ERR_TOO_CLOSE_TO_EDGE",                ScriptError::ERR_TOO_CLOSE_TO_EDGE);
	engine->AddConst ("ERR_STATION_TOO_SPREAD_OUT",           ScriptError::ERR_STATION_TOO_SPREAD_OUT);

	ScriptError::RegisterErrorMap(STR_ERROR_NOT_ENOUGH_CASH_REQUIRES_CURRENCY,          ScriptError::ERR_NOT_ENOUGH_CASH);
	ScriptError::RegisterErrorMap(STR_ERROR_LOCAL_AUTHORITY_REFUSES_TO_ALLOW_THIS,      ScriptError::ERR_LOCAL_AUTHORITY_REFUSES);
	ScriptError::RegisterErrorMap(STR_ERROR_ALREADY_BUILT,                              ScriptError::ERR_ALREADY_BUILT);
	ScriptError::RegisterErrorMap(STR_ERROR_MUST_DEMOLISH_BRIDGE_FIRST,                 ScriptError::ERR_ALREADY_BUILT);
	ScriptError::RegisterErrorMap(STR_ERROR_BUILDING_MUST_BE_DEMOLISHED,                ScriptError::ERR_AREA_NOT_CLEAR);
	ScriptError::RegisterErrorMap(STR_ERROR_MUST_DEMOLISH_BRIDGE_FIRST,                 ScriptError::ERR_AREA_NOT_CLEAR);
	ScriptError::RegisterErrorMap(STR_ERROR_MUST_DEMOLISH_RAILROAD,                     ScriptError::ERR_AREA_NOT_CLEAR);
	ScriptError::RegisterErrorMap(STR_ERROR_MUST_DEMOLISH_AIRPORT_FIRST,                ScriptError::ERR_AREA_NOT_CLEAR);
	ScriptError::RegisterErrorMap(STR_ERROR_MUST_DEMOLISH_CARGO_TRAM_STATION_FIRST,     ScriptError::ERR_AREA_NOT_CLEAR);
	ScriptError::RegisterErrorMap(STR_ERROR_MUST_DEMOLISH_TRUCK_STATION_FIRST,          ScriptError::ERR_AREA_NOT_CLEAR);
	ScriptError::RegisterErrorMap(STR_ERROR_MUST_DEMOLISH_PASSENGER_TRAM_STATION_FIRST, ScriptError::ERR_AREA_NOT_CLEAR);
	ScriptError::RegisterErrorMap(STR_ERROR_MUST_DEMOLISH_BUS_STATION_FIRST,            ScriptError::ERR_AREA_NOT_CLEAR);
	ScriptError::RegisterErrorMap(STR_ERROR_BUOY_IN_THE_WAY,                            ScriptError::ERR_AREA_NOT_CLEAR);
	ScriptError::RegisterErrorMap(STR_ERROR_MUST_DEMOLISH_DOCK_FIRST,                   ScriptError::ERR_AREA_NOT_CLEAR);
	ScriptError::RegisterErrorMap(STR_ERROR_GENERIC_OBJECT_IN_THE_WAY,                  ScriptError::ERR_AREA_NOT_CLEAR);
	ScriptError::RegisterErrorMap(STR_ERROR_COMPANY_HEADQUARTERS_IN,                    ScriptError::ERR_AREA_NOT_CLEAR);
	ScriptError::RegisterErrorMap(STR_ERROR_OBJECT_IN_THE_WAY,                          ScriptError::ERR_AREA_NOT_CLEAR);
	ScriptError::RegisterErrorMap(STR_ERROR_MUST_REMOVE_ROAD_FIRST,                     ScriptError::ERR_AREA_NOT_CLEAR);
	ScriptError::RegisterErrorMap(STR_ERROR_MUST_REMOVE_RAILROAD_TRACK,                 ScriptError::ERR_AREA_NOT_CLEAR);
	ScriptError::RegisterErrorMap(STR_ERROR_MUST_DEMOLISH_BRIDGE_FIRST,                 ScriptError::ERR_AREA_NOT_CLEAR);
	ScriptError::RegisterErrorMap(STR_ERROR_MUST_DEMOLISH_TUNNEL_FIRST,                 ScriptError::ERR_AREA_NOT_CLEAR);
	ScriptError::RegisterErrorMap(STR_ERROR_EXCAVATION_WOULD_DAMAGE,                    ScriptError::ERR_AREA_NOT_CLEAR);
	ScriptError::RegisterErrorMap(STR_ERROR_AREA_IS_OWNED_BY_ANOTHER,                   ScriptError::ERR_OWNED_BY_ANOTHER_COMPANY);
	ScriptError::RegisterErrorMap(STR_ERROR_OWNED_BY,                                   ScriptError::ERR_OWNED_BY_ANOTHER_COMPANY);
	ScriptError::RegisterErrorMap(STR_ERROR_NAME_MUST_BE_UNIQUE,                        ScriptError::ERR_NAME_IS_NOT_UNIQUE);
	ScriptError::RegisterErrorMap(STR_ERROR_FLAT_LAND_REQUIRED,                         ScriptError::ERR_FLAT_LAND_REQUIRED);
	ScriptError::RegisterErrorMap(STR_ERROR_LAND_SLOPED_IN_WRONG_DIRECTION,             ScriptError::ERR_LAND_SLOPED_WRONG);
	ScriptError::RegisterErrorMap(STR_ERROR_TRAIN_IN_THE_WAY,                           ScriptError::ERR_VEHICLE_IN_THE_WAY);
	ScriptError::RegisterErrorMap(STR_ERROR_ROAD_VEHICLE_IN_THE_WAY,                    ScriptError::ERR_VEHICLE_IN_THE_WAY);
	ScriptError::RegisterErrorMap(STR_ERROR_SHIP_IN_THE_WAY,                            ScriptError::ERR_VEHICLE_IN_THE_WAY);
	ScriptError::RegisterErrorMap(STR_ERROR_AIRCRAFT_IN_THE_WAY,                        ScriptError::ERR_VEHICLE_IN_THE_WAY);
	ScriptError::RegisterErrorMap(STR_ERROR_SITE_UNSUITABLE,                            ScriptError::ERR_SITE_UNSUITABLE);
	ScriptError::RegisterErrorMap(STR_ERROR_TOO_CLOSE_TO_EDGE_OF_MAP,                   ScriptError::ERR_TOO_CLOSE_TO_EDGE);
	ScriptError::RegisterErrorMap(STR_ERROR_STATION_TOO_SPREAD_OUT,                     ScriptError::ERR_STATION_TOO_SPREAD_OUT);

	ScriptError::RegisterErrorMapString(ScriptError::ERR_NONE,                             "ERR_NONE");
	ScriptError::RegisterErrorMapString(ScriptError::ERR_UNKNOWN,                          "ERR_UNKNOWN");
	ScriptError::RegisterErrorMapString(ScriptError::ERR_PRECONDITION_FAILED,              "ERR_PRECONDITION_FAILED");
	ScriptError::RegisterErrorMapString(ScriptError::ERR_PRECONDITION_STRING_TOO_LONG,     "ERR_PRECONDITION_STRING_TOO_LONG");
	ScriptError::RegisterErrorMapString(ScriptError::ERR_PRECONDITION_TOO_MANY_PARAMETERS, "ERR_PRECONDITION_TOO_MANY_PARAMETERS");
	ScriptError::RegisterErrorMapString(ScriptError::ERR_PRECONDITION_INVALID_COMPANY,     "ERR_PRECONDITION_INVALID_COMPANY");
	ScriptError::RegisterErrorMapString(ScriptError::ERR_NEWGRF_SUPPLIED_ERROR,            "ERR_NEWGRF_SUPPLIED_ERROR");
	ScriptError::RegisterErrorMapString(ScriptError::ERR_NOT_ENOUGH_CASH,                  "ERR_NOT_ENOUGH_CASH");
	ScriptError::RegisterErrorMapString(ScriptError::ERR_LOCAL_AUTHORITY_REFUSES,          "ERR_LOCAL_AUTHORITY_REFUSES");
	ScriptError::RegisterErrorMapString(ScriptError::ERR_ALREADY_BUILT,                    "ERR_ALREADY_BUILT");
	ScriptError::RegisterErrorMapString(ScriptError::ERR_AREA_NOT_CLEAR,                   "ERR_AREA_NOT_CLEAR");
	ScriptError::RegisterErrorMapString(ScriptError::ERR_OWNED_BY_ANOTHER_COMPANY,         "ERR_OWNED_BY_ANOTHER_COMPANY");
	ScriptError::RegisterErrorMapString(ScriptError::ERR_NAME_IS_NOT_UNIQUE,               "ERR_NAME_IS_NOT_UNIQUE");
	ScriptError::RegisterErrorMapString(ScriptError::ERR_FLAT_LAND_REQUIRED,               "ERR_FLAT_LAND_REQUIRED");
	ScriptError::RegisterErrorMapString(ScriptError::ERR_LAND_SLOPED_WRONG,                "ERR_LAND_SLOPED_WRONG");
	ScriptError::RegisterErrorMapString(ScriptError::ERR_VEHICLE_IN_THE_WAY,               "ERR_VEHICLE_IN_THE_WAY");
	ScriptError::RegisterErrorMapString(ScriptError::ERR_SITE_UNSUITABLE,                  "ERR_SITE_UNSUITABLE");
	ScriptError::RegisterErrorMapString(ScriptError::ERR_TOO_CLOSE_TO_EDGE,                "ERR_TOO_CLOSE_TO_EDGE");
	ScriptError::RegisterErrorMapString(ScriptError::ERR_STATION_TOO_SPREAD_OUT,           "ERR_STATION_TOO_SPREAD_OUT");

	SQConvert::DefSQStaticMethod (engine, &ScriptError::GetErrorCategory,   "GetErrorCategory",   1, ".");
	SQConvert::DefSQStaticMethod (engine, &ScriptError::GetLastError,       "GetLastError",       1, ".");
	SQConvert::DefSQStaticMethod (engine, &ScriptError::GetLastErrorString, "GetLastErrorString", 1, ".");

	engine->AddClassEnd();
}
