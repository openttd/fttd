/* $Id$ */

/*
 * This file is part of OpenTTD.
 * OpenTTD is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 2.
 * OpenTTD is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details. You should have received a copy of the GNU General Public License along with OpenTTD. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file townname_type.h
 * Definition of structures used for generating town names.
 */

#ifndef TOWNNAME_TYPE_H
#define TOWNNAME_TYPE_H

#include <set>
#include <string>

typedef std::set<std::string> TownNames;

/**
 * Struct holding a parameters used to generate town name.
 * Speeds things up a bit because these values are computed only once per name generation.
 */
struct TownNameParams {
	uint32 grfid; ///< newgrf ID (0 if not used)
	uint16 type;  ///< town name style

	/** Empty constructor. */
	TownNameParams (void) : grfid (0), type (0)
	{
	}

	TownNameParams (byte town_name);
};

#endif /* TOWNNAME_TYPE_H */
