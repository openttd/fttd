/* $Id$ */

/*
 * This file is part of OpenTTD.
 * OpenTTD is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 2.
 * OpenTTD is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details. You should have received a copy of the GNU General Public License along with OpenTTD. If not, see <http://www.gnu.org/licenses/>.
 */

/** @file subsidy_gui.cpp GUI for subsidies. */

#include "stdafx.h"
#include "industry.h"
#include "town.h"
#include "window_gui.h"
#include "strings_func.h"
#include "date_func.h"
#include "viewport_func.h"
#include "gui.h"
#include "subsidy_func.h"
#include "subsidy_base.h"
#include "core/geometry_func.hpp"

#include "widgets/subsidy_widget.h"

#include "table/strings.h"

/**
 * Setup the string parameters for printing an end of a subsidy at the screen.
 * @param i Param index to use for the type (id will be next).
 * @param src %CargoSource being printed.
 */
static void SetupSubsidyDecodeParam (uint i, const CargoSource &src)
{
	SetDParam (i, (src.type == ST_INDUSTRY) ?
			STR_INDUSTRY_NAME : STR_TOWN_NAME);
	SetDParam (i + 1, src.id);
}

/**
 * Setup the string parameters for printing the subsidy at the screen.
 * @param s %Subsidy being printed.
 */
static void SetupSubsidyDecodeParams (const Subsidy *s)
{
	SetDParam (0, CargoSpec::Get(s->cargo_type)->name);
	SetupSubsidyDecodeParam (1, s->src);
	SetupSubsidyDecodeParam (4, s->dst);
}

static inline TileIndex GetSubsidyTile (const CargoSource &src)
{
	switch (src.type) {
		case ST_INDUSTRY: return Industry::Get(src.id)->location.tile;
		case ST_TOWN:     return     Town::Get(src.id)->xy;
		default: NOT_REACHED();
	}
}

static inline void HandleClick (const Subsidy *s)
{
	/* determine src coordinate for subsidy and try to scroll to it */
	TileIndex xy = GetSubsidyTile (s->src);

	if (_ctrl_pressed || !ScrollMainWindowToTile(xy)) {
		if (_ctrl_pressed) ShowExtraViewPortWindow (xy);

		/* otherwise determine dst coordinate for subsidy and scroll to it */
		xy = GetSubsidyTile (s->dst);

		if (_ctrl_pressed) {
			ShowExtraViewPortWindow (xy);
		} else {
			ScrollMainWindowToTile (xy);
		}
	}
}

struct SubsidyListWindow : Window {
	Scrollbar *vscroll;

	SubsidyListWindow (const WindowDesc *desc, WindowNumber window_number) :
		Window (desc), vscroll (NULL)
	{
		this->CreateNestedTree();
		this->vscroll = this->GetScrollbar(WID_SUL_SCROLLBAR);
		this->InitNested(window_number);
		this->OnInvalidateData(0);
	}

	virtual void OnClick(Point pt, int widget, int click_count)
	{
		if (widget != WID_SUL_PANEL) return;

		int y = this->vscroll->GetScrolledRowFromWidget(pt.y, this, WID_SUL_PANEL, WD_FRAMERECT_TOP);
		if (y == 0) return; // "Subsidies on offer for services taking:"
		y--;

		int num = 0;
		const Subsidy *s;
		FOR_ALL_SUBSIDIES(s) {
			if (!s->IsAwarded()) {
				if (y == 0) {
					HandleClick (s);
					return;
				}
				y--;
				num++;
			}
		}

		int skip = (num == 0) ?
				3 : // "None"
				2;  // "Services already subsidised:"

		if (y < skip) return;
		y -= skip;

		FOR_ALL_SUBSIDIES(s) {
			if (s->IsAwarded()) {
				if (y == 0) {
					HandleClick (s);
					return;
				}
				y--;
			}
		}
	}

	virtual void UpdateWidgetSize(int widget, Dimension *size, const Dimension &padding, Dimension *fill, Dimension *resize)
	{
		if (widget != WID_SUL_PANEL) return;
		Dimension d = maxdim(GetStringBoundingBox(STR_SUBSIDIES_OFFERED_TITLE), GetStringBoundingBox(STR_SUBSIDIES_SUBSIDISED_TITLE));

		resize->height = d.height;

		d.height *= 5;
		d.width += padding.width + WD_FRAMERECT_RIGHT + WD_FRAMERECT_LEFT;
		d.height += padding.height + WD_FRAMERECT_TOP + WD_FRAMERECT_BOTTOM;
		*size = maxdim(*size, d);
	}

	void DrawWidget (BlitArea *dpi, const Rect &r, int widget) const OVERRIDE
	{
		if (widget != WID_SUL_PANEL) return;

		YearMonthDay ymd;
		ConvertDateToYMD(_date, &ymd);

		int right = r.right - WD_FRAMERECT_RIGHT;
		int y = r.top + WD_FRAMERECT_TOP;
		int x = r.left + WD_FRAMERECT_LEFT;

		int pos = -this->vscroll->GetPosition();
		const int cap = this->vscroll->GetCapacity();

		/* Section for drawing the offered subsidies */
		if (IsInsideMM(pos, 0, cap)) DrawString (dpi, x, right, y + pos * FONT_HEIGHT_NORMAL, STR_SUBSIDIES_OFFERED_TITLE);
		pos++;

		uint num = 0;
		const Subsidy *s;
		FOR_ALL_SUBSIDIES(s) {
			if (!s->IsAwarded()) {
				if (IsInsideMM(pos, 0, cap)) {
					/* Displays the two offered towns */
					SetupSubsidyDecodeParams (s);
					SetDParam(7, _date - ymd.day + s->remaining * 32);
					DrawString (dpi, x, right, y + pos * FONT_HEIGHT_NORMAL, STR_SUBSIDIES_OFFERED_FROM_TO);
				}
				pos++;
				num++;
			}
		}

		if (num == 0) {
			if (IsInsideMM(pos, 0, cap)) DrawString (dpi, x, right, y + pos * FONT_HEIGHT_NORMAL, STR_SUBSIDIES_NONE);
			pos++;
		}

		/* Section for drawing the already granted subsidies */
		pos++;
		if (IsInsideMM(pos, 0, cap)) DrawString (dpi, x, right, y + pos * FONT_HEIGHT_NORMAL, STR_SUBSIDIES_SUBSIDISED_TITLE);
		pos++;
		num = 0;

		FOR_ALL_SUBSIDIES(s) {
			if (s->IsAwarded()) {
				if (IsInsideMM(pos, 0, cap)) {
					SetupSubsidyDecodeParams (s);
					SetDParam(7, s->awarded);
					SetDParam(8, _date - ymd.day + s->remaining * 32);

					/* Displays the two connected stations */
					DrawString (dpi, x, right, y + pos * FONT_HEIGHT_NORMAL, STR_SUBSIDIES_SUBSIDISED_FROM_TO);
				}
				pos++;
				num++;
			}
		}

		if (num == 0) {
			if (IsInsideMM(pos, 0, cap)) DrawString (dpi, x, right, y + pos * FONT_HEIGHT_NORMAL, STR_SUBSIDIES_NONE);
			pos++;
		}
	}

	virtual void OnResize()
	{
		this->vscroll->SetCapacityFromWidget(this, WID_SUL_PANEL);
	}

	/**
	 * Some data on this window has become invalid.
	 * @param data Information about the changed data.
	 * @param gui_scope Whether the call is done from GUI scope. You may not do everything when not in GUI scope. See #InvalidateWindowData() for details.
	 */
	virtual void OnInvalidateData(int data = 0, bool gui_scope = true)
	{
		if (!gui_scope) return;

		/* Count number of (non) awarded subsidies */
		uint num[2] = { 0, 0 };
		const Subsidy *s;
		FOR_ALL_SUBSIDIES(s) {
			num[s->IsAwarded()]++;
		}

		/* Count the 'none' lines */
		if (num[0] == 0) num[0] = 1;
		if (num[1] == 0) num[1] = 1;

		/* Offered, accepted and an empty line before the accepted ones. */
		this->vscroll->SetCount (num[0] + num[1] + 3);
	}
};

static const NWidgetPart _nested_subsidies_list_widgets[] = {
	NWidget(NWID_HORIZONTAL),
		NWidget(WWT_CLOSEBOX, COLOUR_BROWN),
		NWidget(WWT_CAPTION, COLOUR_BROWN), SetDataTip(STR_SUBSIDIES_CAPTION, STR_TOOLTIP_WINDOW_TITLE_DRAG_THIS),
		NWidget(WWT_SHADEBOX, COLOUR_BROWN),
		NWidget(WWT_DEFSIZEBOX, COLOUR_BROWN),
		NWidget(WWT_STICKYBOX, COLOUR_BROWN),
	EndContainer(),
	NWidget(NWID_HORIZONTAL),
		NWidget(WWT_PANEL, COLOUR_BROWN, WID_SUL_PANEL), SetDataTip(0x0, STR_SUBSIDIES_TOOLTIP_CLICK_ON_SERVICE_TO_CENTER), SetResize(1, 1), SetScrollbar(WID_SUL_SCROLLBAR), EndContainer(),
		NWidget(NWID_VERTICAL),
			NWidget(NWID_VSCROLLBAR, COLOUR_BROWN, WID_SUL_SCROLLBAR),
			NWidget(WWT_RESIZEBOX, COLOUR_BROWN),
		EndContainer(),
	EndContainer(),
};

static WindowDesc::Prefs _subsidies_list_prefs ("list_subsidies");

static const WindowDesc _subsidies_list_desc(
	WDP_AUTO, 500, 127,
	WC_SUBSIDIES_LIST, WC_NONE,
	0,
	_nested_subsidies_list_widgets, lengthof(_nested_subsidies_list_widgets),
	&_subsidies_list_prefs
);


void ShowSubsidiesList()
{
	AllocateWindowDescFront<SubsidyListWindow>(&_subsidies_list_desc, 0);
}
