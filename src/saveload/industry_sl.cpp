/* $Id$ */

/*
 * This file is part of OpenTTD.
 * OpenTTD is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 2.
 * OpenTTD is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details. You should have received a copy of the GNU General Public License along with OpenTTD. If not, see <http://www.gnu.org/licenses/>.
 */

/** @file industry_sl.cpp Code handling saving and loading of industries */

#include "../stdafx.h"
#include "../industry.h"

#include "saveload_buffer.h"
#include "saveload_error.h"
#include "newgrf_sl.h"

static OldPersistentStorage _old_ind_persistent_storage;

static const SaveLoad _industry_desc[] = {
	SLE_VAR(Industry, location.tile,              SLE_FILE_U16 | SLE_VAR_U32,  , ,  0, 5),
	SLE_VAR(Industry, location.tile,              SLE_UINT32,                 0, ,  6,  ),
	SLE_VAR(Industry, location.w,                 SLE_FILE_U8 | SLE_VAR_U16),
	SLE_VAR(Industry, location.h,                 SLE_FILE_U8 | SLE_VAR_U16),
	SLE_REF(Industry, town,                       REF_TOWN),
	SLE_NULL( 2, , , 0, 60),       ///< used to be industry's produced_cargo
	SLE_ARR(Industry, produced_cargo,             SLE_UINT8,  2,              0, , 78,  ),
	SLE_ARR(Industry, incoming_cargo_waiting,     SLE_UINT16, 3,              0, , 70,  ),
	SLE_ARR(Industry, produced_cargo_waiting,     SLE_UINT16, 2),
	SLE_ARR(Industry, production_rate,            SLE_UINT8,  2),
	SLE_NULL( 3, , , 0, 60),       ///< used to be industry's accepts_cargo
	SLE_ARR(Industry, accepts_cargo,              SLE_UINT8,  3,              0, , 78,  ),
	SLE_VAR(Industry, prod_level,                 SLE_UINT8),
	SLE_ARR(Industry, this_month_production,      SLE_UINT16, 2),
	SLE_ARR(Industry, this_month_transported,     SLE_UINT16, 2),
	SLE_ARR(Industry, last_month_pct_transported, SLE_UINT8,  2),
	SLE_ARR(Industry, last_month_production,      SLE_UINT16, 2),
	SLE_ARR(Industry, last_month_transported,     SLE_UINT16, 2),

	SLE_VAR(Industry, counter,                    SLE_UINT16),

	SLE_VAR(Industry, type,                       SLE_UINT8),
	SLE_VAR(Industry, owner,                      SLE_UINT8),
	SLE_VAR(Industry, random_colour,              SLE_UINT8),
	SLE_VAR(Industry, last_prod_year,             SLE_FILE_U8 | SLE_VAR_I32,  , ,   0, 30),
	SLE_VAR(Industry, last_prod_year,             SLE_INT32,                 0, ,  31,   ),
	SLE_VAR(Industry, was_cargo_delivered,        SLE_UINT8),

	SLE_VAR(Industry, founder,                    SLE_UINT8,                 0, ,  70,   ),
	SLE_VAR(Industry, construction_date,          SLE_INT32,                 0, ,  70,   ),
	SLE_VAR(Industry, construction_type,          SLE_UINT8,                 0, ,  70,   ),
	SLE_VAR(Industry, last_cargo_accepted_at,     SLE_INT32,                 0, ,  70,   ),
	SLE_VAR(Industry, selected_layout,            SLE_UINT8,                 0, ,  73,   ),

	SLEG_ARR(_old_ind_persistent_storage.storage, SLE_UINT32, 16,             , ,  76, 160),
	SLE_REF(Industry, psa,                        REF_STORAGE,               0, , 161,    ),

	SLE_NULL(1,  0, 24,  82, 196),
	SLE_VAR(Industry, random,                     SLE_UINT16,                0, ,  82,    ),

	SLE_NULL(32, , , 2, 143), // old reserved space

	SLE_END()
};

static void Save_INDY(SaveDumper *dumper)
{
	Industry *ind;

	/* Write the industries */
	FOR_ALL_INDUSTRIES(ind) {
		dumper->WriteElement(ind->index, ind, _industry_desc);
	}
}

static void Save_IIDS(SaveDumper *dumper)
{
	Save_NewGRFMapping(dumper, _industry_mngr);
}

static void Save_TIDS(SaveDumper *dumper)
{
	Save_NewGRFMapping(dumper, _industile_mngr);
}

static void Load_INDY(LoadBuffer *reader)
{
	int index;

	Industry::ResetIndustryCounts();

	while ((index = reader->IterateChunk()) != -1) {
		Industry *i = new (index) Industry();
		reader->ReadObject(i, _industry_desc);
		i->add_to_tileset();

		/* Before legacy savegame version 161, persistent storages were not stored in a pool. */
		if (reader->IsOTTDVersionBefore(161) && !reader->IsOTTDVersionBefore(76)) {
			/* Store the old persistent storage. The GRFID will be added later. */
			assert(PersistentStorage::CanAllocateItem());
			i->psa = new PersistentStorage(0, 0, 0);
			memcpy(i->psa->storage, _old_ind_persistent_storage.storage, sizeof(i->psa->storage));
		}
		Industry::IncIndustryTypeCount(i->type);
	}
}

static void Load_IIDS(LoadBuffer *reader)
{
	Load_NewGRFMapping(reader, _industry_mngr);
}

static void Load_TIDS(LoadBuffer *reader)
{
	Load_NewGRFMapping(reader, _industile_mngr);
}

static void Ptrs_INDY(const SavegameTypeVersion *stv)
{
	Industry *i;

	FOR_ALL_INDUSTRIES(i) {
		SlObjectPtrs(i, _industry_desc, stv);
	}
}

/** Save industry builder. */
static void Save_IBLD(SaveDumper *dumper)
{
	dumper->WriteRIFFSize (sizeof(uint32));
	dumper->WriteUint32 (_industry_builder.wanted_inds);
}

/** Load industry builder. */
static void Load_IBLD(LoadBuffer *reader)
{
	_industry_builder.wanted_inds = reader->ReadUint32();
}

/** Description of the data to save and load in #IndustryTypeBuildData. */
static const SaveLoad _industrytype_builder_desc[] = {
	SLE_VAR(IndustryTypeBuildData, probability,  SLE_UINT32),
	SLE_VAR(IndustryTypeBuildData, min_number,   SLE_UINT8),
	SLE_VAR(IndustryTypeBuildData, target_count, SLE_UINT16),
	SLE_VAR(IndustryTypeBuildData, max_wait,     SLE_UINT16),
	SLE_VAR(IndustryTypeBuildData, wait_count,   SLE_UINT16),
	SLE_END()
};

/** Save industry-type build data. */
static void Save_ITBL(SaveDumper *dumper)
{
	for (int i = 0; i < NUM_INDUSTRYTYPES; i++) {
		dumper->WriteElement(i, _industry_builder.builddata + i, _industrytype_builder_desc);
	}
}

/** Load industry-type build data. */
static void Load_ITBL(LoadBuffer *reader)
{
	for (IndustryType it = 0; it < NUM_INDUSTRYTYPES; it++) {
		_industry_builder.builddata[it].Reset();
	}
	int index;
	while ((index = reader->IterateChunk()) != -1) {
		if ((uint)index >= NUM_INDUSTRYTYPES) throw SlCorrupt("Too many industry builder datas");
		reader->ReadObject(_industry_builder.builddata + index, _industrytype_builder_desc);
	}
}

extern const ChunkHandler _industry_chunk_handlers[] = {
	{ 'INDY', Save_INDY,     Load_INDY,     Ptrs_INDY, NULL, CH_ARRAY},
	{ 'IIDS', Save_IIDS,     Load_IIDS,     NULL,      NULL, CH_ARRAY},
	{ 'TIDS', Save_TIDS,     Load_TIDS,     NULL,      NULL, CH_ARRAY},
	{ 'IBLD', Save_IBLD,     Load_IBLD,     NULL,      NULL, CH_RIFF},
	{ 'ITBL', Save_ITBL,     Load_ITBL,     NULL,      NULL, CH_ARRAY | CH_LAST},
};
